module.exports = () => ({
  'mail-formdata': {
    enabled: true,
    resolve: './src/plugins/mail-formdata'
  },
});
