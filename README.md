# Strapi Plugin Mail Formdata

In this repo, Strapi was installed in an empty directory in the `playground`` subdirectory. A plugin with the name `mail-formdata`` was created in the Strapi installation. 

Practically per:

```
yarn create strapi-app playground 
cd playground
yarn strapi generate
```

The goal is to have a Strapi plugin that can be easily developed by checking out the repo and starting a Strapi with an integrated module. The idea is inspired by https://github.com/pluginpal/strapi-plugin-config-sync

Currently you have to build the plugin before you can start Strapi via:

```
git clone https://gitlab.com/cross-solution/strapi/strapi-plugin-mail-formdata.git
cd strapi-plugin-mail-formdata/playground
yarn
cd src/plugins/mail-formdata
yarn
yarn build
cd ../../..
yarn 
cd src/plugins/mail-formdata
yarn build
cd ../../..
yarn develop
```



